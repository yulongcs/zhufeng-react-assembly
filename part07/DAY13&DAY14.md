## Progress

 还是老样子，初始化组件，然后分析下这个组件应该有什么功能。

一般来说，进度条有2种，一种长条形，一种环形。

长条形用div就可搞定，环形需要使用svg来制作。

另外，进度可以操纵就不说了，文字应该可以自定义。

### svg逻辑

由于我们需要搞个圆环进度条，所以需要了解下svg的知识。

svg的viewBox控制着内部图像比例

defs标签有点像定义变量一样，可以定义后面要使用的形状。

defs mdn https://developer.mozilla.org/zh-CN/docs/Web/SVG/Element/defs

circle标签可以直接让我们画出个圆。

circle mdn https://developer.mozilla.org/zh-CN/docs/Web/SVG/Element/circle

circle中cx cy就是半径，r是内半径，相对于viewbox大小的。

一般 r+strokeWidth=cx||cy，可以理解cx cy是包含边框。

还有个比较重要的属性叫strokeDasharray，通过这个属性可以控制圆环的进度。

strokeDasharray mdn https://developer.mozilla.org/zh-CN/docs/Web/SVG/Attribute/stroke-dasharray

这个属性顾名思义是画虚线的，怎么画成一段，就靠计算间隔和画的长度，笔画长度需要用圆周长*百分比，间隔长度要大于剩余百分比即可，也是用圆周长计算。

### 基本框架

先制作基本框架：

```typescript
export type ProgressProps = {
	/** 传入数字*/
	count: number;
	/** 是否要末尾计数文本*/
	countNumber?: boolean;
	/** 环状不生效 进度条高度*/
	height?: number;
	/** 是否是环状*/
	cicrle?: boolean;
	/** 环状才生效 环状大小*/
	size?: number;
	/**自定义环状进度条文本内容 */
	circleText?: ReactNode;
	/** 自定义长条进度条文本内容*/
	progressText?: ReactNode;
};

export function Progress(props: PropsWithChildren<ProgressProps>) {
	const {
		count,
		countNumber,
		height,
		cicrle,
		size,
		circleText,
		progressText,
	} = props;
	const [state, setState] = useState(0);
	const [dasharray, setdashArray] = useState("");
	useMemo(() => {
		if (count < 0) {
			setState(0);
		} else if (count > 100) {
			setState(100);
		} else {
			setState(count);
		}
	}, [count]);
	useEffect(() => {
		if (cicrle) {
			let percent = state / 100;
			let perimeter = Math.PI * 2 * 170; //周长
			let dasharray =
				perimeter * percent + " " + perimeter * (1 - percent);
			setdashArray(dasharray);
		}
	}, [cicrle, state]);
	const render = useMemo(() => {
		if (cicrle) {
			return (
				<div>
					<svg
						width={size}
						height={size}
						viewBox="0 0 420 420"
						style={{ transform: "rotate(270deg)" }}
					>
						<defs>
							<radialGradient
								id="linear"
								r="100%"
								cx="100%"
								cy="100%"
								spreadMethod="pad"
							>
								<stop offset="0%" stopColor="#40a9ff" />
								<stop offset="100%" stopColor="#36cfc9" />
							</radialGradient>
						</defs>
						<circle
							cx="210"
							cy="210"
							r="170"
							strokeWidth="40"
							stroke="#f5f5f5"
							fill="none"
						></circle>
						<circle
							cx="210"
							cy="210"
							r="170"
							strokeWidth="40"
							stroke="url(#linear)"
							fill="none"
							opacity={state === 0 ? 0 : 1}
							strokeLinecap="round"
							strokeDasharray={dasharray}
							strokeDashoffset={"0px"}
							style={{
								transition:
									"stroke-dashoffset 0.3s ease 0s, stroke-dasharray 0.3s ease 0s, stroke 0.3s ease 0s, stroke-width 0.06s ease 0.3s",
							}}
						></circle>
					</svg>
					<div
						style={{
							lineHeight: `${size! * 0.62}px`,
							width: `${size! * 0.62}px`,
							height: `${size! * 0.62}px`,
						}}
					>
						{circleText ? circleText : `${state}%`}
					</div>
				</div>
			);
		} else {
			return (
				<div style={{ display: "flex" }}>
					<div style={{ width: "100%" }}>
						<div
							style={{
								width: `${state}%`,
								height: `${height ? height : 8}px`,
								background: "red",
							}}
						></div>
					</div>
					{countNumber && (
						<div style={{ lineHeight: `${height ? height : 8}px` }}>
							{progressText ? progressText : `${state}%`}
						</div>
					)}
				</div>
			);
		}
	}, [
		cicrle,
		circleText,
		countNumber,
		dasharray,
		height,
		progressText,
		size,
		state,
	]);

	return <>{render}</>;
}

Progress.defaultProps = {
	countNumber: true,
	cicrle: false,
	size: 100,
};

```

story处可以测试下：

```typescript
export const knobsProgress = () => <Progress count={20}></Progress>;

export const test = () => <Progress count={20} cicrle={true}></Progress>;
```

### 美化样式

外层容器改为styledcomponent，并暴露style和classname。

文字部分需要进行校准。

长条进度跳要把颜色改改，做成可配置的。

为了使进度条更炫，还需要写个动画，这个动画是抄antd的：

```typescript
export const progressFlash = keyframes`
  0% { opacity: 0.1;
    width: 0; 
  }
  20% { opacity: 0.5;
    width: 0; 
  } 
  100% { opacity: 0;
    width: 100%; 
  }
`;
```

写在动画文件里。

加入styledComponents

```typescript
const BarWrapper = styled.div`
	display: flex;
	padding: 5px;
	align-items: center;
`;

interface BarMainProps {
	state: number;
	height?: number;
	flashColor: string;
	primary: string;
	secondary: string;
}

const BarMain = styled.div<BarMainProps>`
	width: ${(props) => props.state}%;
	height: ${(props) => (props.height ? props.height : 8)}px;
	background-color: ${(props) => props.primary};
	background-image: linear-gradient(
		to right,
		${(props) => props.primary},
		${(props) => props.secondary}
	);
	transition: all 0.4s cubic-bezier(0.08, 0.82, 0.17, 1) 0s;
	border-radius: 5px;
	position: relative;
	&::before {
		animation: ${progressFlash} 2.4s cubic-bezier(0.23, 1, 0.32, 1) infinite;
		background: ${(props) => props.flashColor};
		border-radius: 10px;
		bottom: 0;
		content: "";
		left: 0;
		opacity: 0;
		position: absolute;
		right: 0;
		top: 0;
	}
`;
const BarMainWrapper = styled.div<{ bottomColor: string; height?: number }>`
	width: 100%;
	border-radius: 5px;
	position: relative;
	background: ${(props) => props.bottomColor};
	height: ${(props) => (props.height ? props.height : 8)}px;
`;
const BarText = styled.div<{ height?: number }>`
	line-height: ${(props) => (props.height ? props.height : 8)}px;
	font-weight: ${typography.weight.bold};
	text-align: center;
	display: inline-block;
	margin-left: 10px;
	min-width: 55px;
`;
const CircleWrapper = styled.div`
	position: relative;
	display: inline-block;
	border-radius: 50%;
`;

const CircleText = styled.div<{ size: number }>`
	line-height: ${(props) => props.size * 0.62}px;
	width: ${(props) => props.size * 0.62}px;
	height: ${(props) => props.size * 0.62}px;
	border-radius: 50%;
	display: inline-block;
	font-weight: ${typography.weight.bold};
	left: 50%;
	position: absolute;
	text-align: center;
	top: 50%;
	transform: translateX(-50%) translateY(-50%);
`;
```

修改传入属性：

```typescript
export type ProgressProps = {
	/** 传入数字*/
	count: number;
	/** 是否要末尾计数文本*/
	countNumber?: boolean;
	/** 环状不生效 进度条高度*/
	height?: number;
	/** 是否是环状*/
	circle?: boolean;
	/** 环状才生效 环状大小*/
	size?: number;
	/** 自定义文本内容*/
	progressText?: ReactNode;
	/** 长条闪烁动画颜色 */
	flashColor?: string;
	/** 主色 */
	primary?: string;
	/** 副色 */
	secondary?: string;
	/** 底座色 */
	bottomColor?: string;
	/** 外层容器style*/
	style?: CSSProperties;
	/** 外层容器类名 */
	classname?: string;
};
```

布局：

```typescript
export function Progress(props: ProgressProps) {
	const {
		count,
		countNumber,
		height,
		circle,
		size,
		progressText,
		flashColor,
		primary,
		secondary,
		bottomColor,
		style,
		classname,
	} = props;
	const [state, setState] = useState(0);
	const [dasharray, setdashArray] = useState("");
	useMemo(() => {
		if (count < 0) {
			setState(0);
		} else if (count > 100) {
			setState(100);
		} else {
			setState(count);
		}
	}, [count]);
	useEffect(() => {
		if (circle) {
			let percent = state / 100;
			let perimeter = Math.PI * 2 * 170; //周长
			let dasharray =
				perimeter * percent + " " + perimeter * (1 - percent);
			setdashArray(dasharray);
		}
	}, [circle, state]);

	const render = useMemo(() => {
		if (circle) {
			return (
				<CircleWrapper style={style} className={classname}>
					<svg
						width={size}
						height={size}
						viewBox="0 0 420 420"
						style={{
							transform: "rotate(270deg)",
						}}
					>
						<defs>
							<radialGradient
								id="linear"
								r="100%"
								cx="100%"
								cy="100%"
								spreadMethod="pad"
							>
								<stop offset="0%" stopColor={primary} />
								<stop offset="100%" stopColor={secondary} />
							</radialGradient>
						</defs>
						<circle
							cx="210"
							cy="210"
							r="170"
							strokeWidth="40"
							stroke={bottomColor}
							fill="none"
						></circle>
						<circle
							cx="210"
							cy="210"
							r="170"
							strokeWidth="40"
							stroke="url(#linear)"
							fill="none"
							opacity={state === 0 ? 0 : 1}
							strokeLinecap="round"
							strokeDasharray={dasharray}
							strokeDashoffset={"0px"}
							style={{
								transition:
									"stroke-dashoffset 0.3s ease 0s, stroke-dasharray 0.3s ease 0s, stroke 0.3s ease 0s, stroke-width 0.06s ease 0.3s",
							}}
						></circle>
					</svg>
					<CircleText size={size!}>
						{progressText ? progressText : `${state}%`}
					</CircleText>
				</CircleWrapper>
			);
		} else {
			return (
				<BarWrapper style={style} className={classname}>
					<BarMainWrapper bottomColor={bottomColor!} height={height}>
						<BarMain
							flashColor={flashColor!}
							primary={primary!}
							secondary={secondary!}
							state={state}
							height={height}
						></BarMain>
					</BarMainWrapper>
					{countNumber && (
						<BarText height={height}>
							{progressText ? progressText : `${state}%`}
						</BarText>
					)}
				</BarWrapper>
			);
		}
	}, [
		circle,
		countNumber,
		dasharray,
		flashColor,
		height,
		primary,
		progressText,
		secondary,
		size,
		state,
		bottomColor,
		style,
		classname,
	]);

	return <>{render}</>;
}
```

default属性：

```typescript
Progress.defaultProps = {
	countNumber: true,
	circle: false,
	size: 100,
	primary: color.primary,
	secondary: color.gold,
	flashColor: color.lightest,
	bottomColor: color.medium,
};
```

这样差不多可以了，下面编写story。

### 编写STORY

同样的，开局写个knob story：

```typescript
export const knobsProgress = () => (
	<Progress
		count={number("count", 50, { range: true, min: 0, max: 100, step: 1 })}
		countNumber={boolean("countNumber", true)}
		height={number("height", 8)}
		circle={boolean("circle", false)}
		size={number("size", 100)}
		primary={color("primary", "#FF4785")}
		secondary={color("secondary", "#FFAE00")}
		bottomColor={color("bottomColor", "#DDDDDD")}
		flashColor={color("flashColor", "#FFFFFF")}
		progressText={text("progressText", "")}
	></Progress>
);
```

后面进行展示下其他状态就可以了：

```typescript
export const circle = () => <Progress count={80} circle={true}></Progress>;

export const progressText = () => (
	<Progress count={11} progressText={"yehuozhili"}></Progress>
);

export const changeColor = () => (
	<Progress
		count={20}
		primary="blue"
		secondary="yellow"
		bottomColor="brown"
	></Progress>
);

export const withIcon = () => (
	<Progress
		count={11}
		progressText={
			<span>
				<Icon icon="admin"></Icon>
			</span>
		}
	></Progress>
);
```

### 编写测试

展示组件测试比较好写，各种状态展示下截个图就完事了。

```typescript
import React from "react";
import { render, cleanup } from "@testing-library/react";
import { Progress } from "../index";
import { color } from "../../shared/styles";

describe("test Progress component", () => {
	it(" should render bycount", () => {
		let wrapper = render(<Progress count={11}></Progress>);
		let text = wrapper.getByText("11%");
		expect(text).toBeTruthy();
		expect(wrapper).toMatchSnapshot();
		cleanup();
		wrapper = render(<Progress count={122}></Progress>);
		expect(wrapper).toMatchSnapshot();
		text = wrapper.getByText("100%");
		expect(text).toBeTruthy();
		cleanup();
		wrapper = render(<Progress count={-12}></Progress>);
		expect(wrapper).toMatchSnapshot();
		text = wrapper.getByText("0%");
		expect(text).toBeTruthy();
		cleanup();
		wrapper = render(<Progress circle={true} count={11}></Progress>);
		text = wrapper.getByText("11%");
		expect(text).toBeTruthy();
		expect(wrapper).toMatchSnapshot();
		cleanup();
		wrapper = render(<Progress circle={true} count={122}></Progress>);
		expect(wrapper).toMatchSnapshot();
		text = wrapper.getByText("100%");
		expect(text).toBeTruthy();
		cleanup();
		wrapper = render(<Progress circle={true} count={-12}></Progress>);
		expect(wrapper).toMatchSnapshot();
		text = wrapper.getByText("0%");
		expect(text).toBeTruthy();
	});
	it("should change color", () => {
		let wrapper = render(
			<Progress
				count={11}
				primary={color.orange}
				secondary={color.purple}
				bottomColor={color.ultraviolet}
				flashColor={color.seafoam}
			></Progress>
		);
		expect(wrapper).toMatchSnapshot();
		cleanup();
		wrapper = render(
			<Progress
				count={11}
				primary={color.negative}
				secondary={color.seafoam}
				bottomColor={color.gold}
				flashColor={color.mediumdark}
			></Progress>
		);
		expect(wrapper).toMatchSnapshot();
		cleanup();
		wrapper = render(
			<Progress
				circle={true}
				count={11}
				primary={color.orange}
				secondary={color.purple}
				bottomColor={color.ultraviolet}
				flashColor={color.seafoam}
			></Progress>
		);
		expect(wrapper).toMatchSnapshot();
		cleanup();
		wrapper = render(
			<Progress
				circle={true}
				count={11}
				primary={color.negative}
				secondary={color.seafoam}
				bottomColor={color.gold}
				flashColor={color.mediumdark}
			></Progress>
		);
		expect(wrapper).toMatchSnapshot();
	});
	it("should change text ", () => {
		let wrapper = render(
			<Progress count={11} progressText={"yehuozhili"}></Progress>
		);
		let text = wrapper.getByText("yehuozhili");
		expect(text).toBeTruthy();
		cleanup();
		wrapper = render(
			<Progress
				circle={true}
				count={11}
				progressText={"yehuozhili"}
			></Progress>
		);
		text = wrapper.getByText("yehuozhili");
		expect(text).toBeTruthy();
	});
	it("should change size", () => {
		let wrapper = render(<Progress count={11} height={500}></Progress>);
		expect(wrapper).toMatchSnapshot();
		cleanup();
		wrapper = render(
			<Progress circle={true} count={11} size={400}></Progress>
		);
		expect(wrapper).toMatchSnapshot();
	});
});

```

## Modal

### 原理

这个组件的难点主要在布局上，布局采用抄袭antd方式，一块整的fix的div，模态框做margin:auto从而居中，距上方高度写死。就可以了。

第二个问题在停止滚动上，这里有个注意点就是你可能需要减去滚动条长度。

第三个问题就是卸载动画问题。

这里不存在message组件里说的漏卸载问题，因为这个modal就是需要fiber一直存在反复调用的。所以就会有卸载动画问题，需要在modal组件内部维护自己的状态，否则卸载动画不会触发。

挂载方面我们使用createPortal实现，这个是传送门，类似于拿到vdom然后渲染的dom会跑到挂载点上，但是这个vdom仍在当前组件下，而不是在挂载点那个组件上。

### 基本实现

先制作出简易示例。

```typescript
export function Modal(props: PropsWithChildren<ModalProps>) {
	const [visible, setVisible] = useState(false);
	const render = () => {
		if (visible) {
			return <div>11111111</div>;
		} else {
			return null;
		}
	};
	const mount = document.body;

	return (
		<div id="test">
			{createPortal(render(), mount)}
			<button onClick={() => setVisible(!visible)}>++++</button>
		</div>
	);
}
```

### 增加功能

到时候，用户会在父组件调用切换功能，只要在modal里收到父组件传来显示或者关闭即可。

另外，modal还需要自己控制开启和关闭，因为当点击弹出框里内容或者mask要进行自己关闭。如果modal没有关闭动画，modal完全可以使用父组件状态，只需要接收父组件的切换状态方法。如果要自己维护状态，需要自己做个state。

父组件setState类型需要注意下，别写成React.Dispatch，因为这样就限定死函数必须是useState里返回的那个了，而应该设置成更通用类型：

```
(v: boolean) => void
```

这里我们可以使用个自定义hook来完成这个事：

```typescript
export function useStateAnimation(
	parentSetState: (v: boolean) => void,
	delay: number = 300
): [boolean, (v: boolean) => void, () => void] {
	const [state, setState] = useState(true);
	const [innerClose, unmount] = useMemo(() => {
		let timer: number;
		let innerclose = (v: boolean) => {
			setState(v);
			timer = window.setTimeout(() => {
				parentSetState(v);
				setState(true);
			}, delay);
		};
		let unmount = () => window.clearTimeout(timer);
		return [innerclose, unmount];
	}, [setState, parentSetState, delay]);
	return [state, innerClose, unmount];
}
```

注意在delay期间卸载，会产生未处理定时器，所以一起弄出去。

制作个停止滚动的hooks，在modal开启后，如果还能滚动就会显得很怪异，antd的modal打开后就不会滚动，所以我们写个自定义hook解决：

```typescript
export function useStopScroll(state: boolean, delay: number, open?: boolean) {
	if (open) {
		let width = window.innerWidth - document.body.clientWidth;
		if (state) {
			document.body.style.overflow = "hidden";
			document.body.style.width = `calc(100% - ${width}px)`;
		} else {
			//等动画渲染
			setTimeout(() => {
				document.body.style.overflow = "auto";
				document.body.style.width = `100%`;
			}, delay);
		}
	}
}
```

我们需要把这个modal的wrapper分成3部分。

第一部分是标题，第二部分是children，第三部分是confrm就是下面2确认按钮。

标题可传可不传，不传不显示，children必显示，所以设定最小高度。confirm可显示可不显示。

styledcomponents:

```typescript
const ModalWrapper = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 1000;
`;

const ModalViewPort = styled.div<{ visible: boolean; delay: number }>`
	background: #fff;
	border: none;
	border-radius: 5px;
	box-shadow: 2px 2px 4px #d9d9d9;
	text-shadow: 1px 1px 4px #d9d9d9, -1px -1px 4px #fff;
	margin: 0 auto;
	min-width: 320px;
	overflow: hidden;
	padding: 8px;
	position: relative;
	top: 100px;
	transition: all 0.5s cubic-bezier(0.23, 1, 0.32, 1);
	width: 30%;
	z-index: 1001;
	${(props) =>
		props.visible &&
		css`
			animation: ${modalOpenAnimate} ${props.delay / 1000}s ease-in;
		`}
	${(props) =>
		!props.visible &&
		css`
			animation: ${modalCloseAnimate} ${props.delay / 1000}s ease-in;
		`}
`;

const ModalMask = styled.div`
	background-color: rgba(0, 0, 0, 0.45);
	bottom: 0;
	left: 0;
	position: fixed;
	right: 0;
	top: 0;
`;

const CloseBtn = styled.div`
	position: absolute;
	right: 5px;
	top: 5px;
`;

const ConfirmWrapper = styled.div`
	display: flex;
	justify-content: flex-end;
	align-item: center;
	padding: 10px;
`;

const ChildrenWrapper = styled.div`
	min-height: 100px;
	padding: 10px;
`;

const TitleWrapper = styled.div`
	height: 30px;
	line-height: 30px;
	font-size: ${typography.size.m2}px;
	font-weight: ${typography.weight.bold};
	padding: 5px;
`;
```

布局部分：

```typescript
export type ModalProps = {
	/** 父组件用来控制的状态 */
	visible: boolean;
	/** 容器位置 */
	container?: Element;
	/** 父组件setstate */
	parentSetState: (v: boolean) => void;
	/** 弹出框标题 */
	title?: ReactNode;
	/** 是否有确认按钮 */
	confirm?: boolean;
	/** 改变确认按钮文本*/
	okText?: string;
	/** 改变取消按钮文本*/
	cancelText?: string;
	/** 点了确认的回调，如果传了，需要自行处理关闭 */
	onOk?: (set:(v: boolean) => void) => void;
	/** 点了取消的回调，如果传了，需要自行处理关闭*/
	onCancel?: (set:(v: boolean) => void) => void;
	/** 点确认或者取消都会走的回调 */
	callback?: (v: boolean) => void;
	/** 点击mask是否关闭模态框 */
	maskClose?: boolean;
	/** 是否有mask */
	mask?: boolean;
	/** 自定义模态框位置 */
	style?: CSSProperties;
	/** 是否有右上角关闭按钮 */
	closeButton?: boolean;
	/** 动画时间 */
	delay?: number;
	/** 是否停止滚动*/
	stopScroll?: boolean;
	/** portralstyle*/
	portralStyle?: CSSProperties;
	/** portral的回调 */
	refCallback?: (ref: HTMLDivElement) => void;
	/** 没点确认于取消，直接关闭的回调 */
	closeCallback?: () => void;
};

export function Modal(props: PropsWithChildren<ModalProps>) {
	const {
		visible,
		maskClose,
		closeButton,
		delay,
		mask,
		container,
		confirm,
		okText,
		style,
		cancelText,
		onOk,
		onCancel,
		callback,
		title,
		parentSetState,
		stopScroll,
		portralStyle,
		refCallback,
		closeCallback,
	} = props;
	const ref = useRef<HTMLDivElement>(null);

	const [state, setState, unmount] = useStateAnimation(parentSetState, delay);

	const render = useMemo(() => {
		if (!visible) {
			unmount();
			return null;
		} else {
			return createPortal(
				<ModalWrapper ref={ref} style={portralStyle}>
					<ModalViewPort style={style} visible={state} delay={delay!}>
						<div>
							{title && <TitleWrapper>{title}</TitleWrapper>}
							{closeButton && (
								<CloseBtn>
									<Button
										style={{
											background: "white",
											borderRadius: "5px",
											padding: "5px",
										}}
										onClick={() => {
											setState(false);
											if (closeCallback) closeCallback();
										}}
									>
										<Icon icon="closeAlt"></Icon>
									</Button>
								</CloseBtn>
							)}
						</div>
						{<ChildrenWrapper>{props.children}</ChildrenWrapper>}
						{confirm && (
							<ConfirmWrapper>
								<Button
									appearance="secondary"
									onClick={() => {
										onOk ? onOk(setState) : setState(false);
										if (callback) callback(true);
									}}
								>
									{okText ? okText : "确认"}
								</Button>
								<Button
									appearance="secondary"
									onClick={() => {
										onCancel ? onCancel(setState) : setState(false);
										if (callback) callback(false);
									}}
									style={{ marginLeft: "10px" }}
								>
									{cancelText ? cancelText : "取消"}
								</Button>
							</ConfirmWrapper>
						)}
					</ModalViewPort>
					{mask && (
						<ModalMask
							onClick={() => {
								if (maskClose) {
									setState(false);
									if (closeCallback) {
										closeCallback();
									}
								}
							}}
						></ModalMask>
					)}
				</ModalWrapper>,
				container!
			);
		}
	}, [
		callback,
		cancelText,
		closeButton,
		closeCallback,
		confirm,
		container,
		mask,
		maskClose,
		okText,
		onCancel,
		onOk,
		portralStyle,
		props.children,
		setState,
		style,
		title,
		state,
		visible,
		delay,
		unmount,
	]);
	useStopScroll(visible!, 300, stopScroll!);
	useEffect(() => {
		if (refCallback && ref.current) {
			refCallback(ref.current);
		}
	}, [refCallback]);
	return <>{render}</>;
}
```

### 编写Story

为了加快进度，story就写个knob，测试不写，各位自己完成。

```typescript
function KnobsModal() {
	const [state, setState] = useState(false);
	const title = text("title", "标题");
	const child = text("children", "sdsdsssda");
	const confirm = boolean("confirm", true);
	const okText = text("okText", "");
	const cancelText = text("cancelText", "");
	return (
		<div>
			<Modal
				refCallback={action("refcallback")}
				stopScroll={boolean("stopScroll", true)}
				delay={number("delay", 200)}
				closeButton={boolean("closeButton", true)}
				mask={boolean("mask", true)}
				maskClose={boolean("maskClose", true)}
				callback={action("callback")}
				cancelText={cancelText}
				okText={okText}
				confirm={confirm}
				title={title}
				parentSetState={setState}
				visible={state}
			>
				{child}
			</Modal>
			<Button onClick={() => setState(!state)}>toggle</Button>
		</div>
	);
}

export const knobsModal = () => <KnobsModal></KnobsModal>;
```



##  今日作业

完成progress与modal

### 可选作业

我们来做一个message组件，这个组件很常用。

这个组件很多人都用rc-notification包装一下，包括antd也是这么搞，我们肯定得手写，所以需要梳理下原理。

### 原理

我自己那个bigbear组件库写的那个message组件没写好，主要原因是少卸载fiber，dom确实卸载了，这个可能导致内存泄漏（这个效果估计得触发超过10w遍且一直不刷新页面才能看得出来）。

这个首要的难点就是组件卸载问题，所以组件它除了要卸载掉dom，fiber也得卸载掉。

那么如何卸载？就是reactdom提供的unmount方法，这个方法比较恶心，我一开始写这个组件时候试过这个，但是效果有点不太对，我用法有问题，然后就没去卸载它。它应该是卸载reactDom.render的一整颗树。而render大家都用过，需要有挂载点，会清除挂载点里内容，所以这里我们肯定不能直接挂body上，不然把body都干掉了。所以这里的逻辑就是造个div，然后把内容挂这个div上，unmount就会卸载掉div里内容，然后你还得卸载掉这个div（因为这个div也是新加的）。也就是需要卸载2个东西。

解决了卸载问题，还有个难点，就是一个可以自动获得不同高度的问题。这个问题有几种解决方法，有的是维护一个队列，然后算高度，我看了最简单最实用的方法是做一个空div定到顶部，fix定位，然后point-event:none让其不能选中，关键点在这，后面加的所有子div实际都是这个fix元素的子元素，这样就能让其自动获得对应高度了。

### 初步实现

我们先写个简易版本，让大家感受下原理：

老样子，初始化message组件，index：

```typescript
let wrap: HTMLElement;
export const createMessage = () => {
	return (content: ReactNode) => {
		if (!wrap) {
			//如果有的话，说明已经调用过这个函数了，这个空div就可以一直复用
			wrap = document.createElement("div");
			wrap.style.cssText = `line-height:
		1.5;text-align:
		center;color: #333;
		box-sizing: border-box;
		margin: 0;
		padding: 0;
		list-style: none;
		position: fixed;
		z-index: 100000;
		width: 100%;
		top: 16px;
		left: 0;
		pointer-events: none;`;
            if (wrap) {
			document.body && document.body.appendChild(wrap); //挂body上
		}
		}

		
		const divs = document.createElement("div");
		wrap.appendChild(divs);
		ReactDom.render(
			<Message rootDom={wrap} parentDom={divs} content={content} />,
			divs
		);
	};
};

export type MessageProps = {
	rootDom: HTMLElement; //这个用来干掉parentDom 这个可以常驻
	parentDom: Element | DocumentFragment; //这个是挂载点 要unmount卸载 完毕后卸载挂载点 注意！一共2步卸载，别漏了
	content: ReactNode;
};

export function Message(props: MessageProps) {
	const { rootDom, parentDom, content } = props;

	const unmount = useMemo(() => {
		return () => {
			if (parentDom && rootDom) {
				unmountComponentAtNode(parentDom);
				rootDom.removeChild(parentDom);
			}
		};
	}, [parentDom, rootDom]);

	useEffect(() => {
		setTimeout(() => {
			unmount();
		}, 2000);
	}, [unmount]);
	return <div style={{ background: "red" }}>{content}</div>;
```

 使用story进行验证：

```typescript
export const knobsMessage = () => (
	<div>
		<Button onClick={() => createMessage()("111")}>click</Button>
		<Button onClick={() => createMessage()("222")}>click</Button>
	</div>
);
```

可以发现，这样就既实现了卸载问题，又实现了类似队列的行为。

### 功能与美化

完成了基本实现，下面需要暴露可配置参数给user了。

首先，我们使用antd的message都会用message.success或者message.error之类来调用，对应的显示内容会有对应的icon。所以第一个参数传文本，第二个参数传配置项比较好。

制作动画：

```typescript
export const messageOpenAnimate = keyframes`
  0% {
    opacity: 0;
    margin-top: -30px;
  }
  50% {
    opacity: 0.1;
    margin-top: -15px;
  }
  100% {
    opacity: 1;
    margin-top: 0;
  }
`;

export const messageCloseAnimate = keyframes`
  0% {
    opacity: 1;
    margin-top: 0;
  }
  100% {
    opacity: 0;
    margin-top: -30px;
  }
`;
export const iconSpin = keyframes`
  0% {
     transform: rotate(0deg);
  }
  100% {
     transform: rotate(360deg);
  }
`;
```

style里加入box-shadow，这个box-shadow是antd抄来的：

```
export const messageBoxShadow = css`
	box-shadow: 0 3px 6px -4px rgba(0, 0, 0, 0.12),
		0 6px 16px 0 rgba(0, 0, 0, 0.08), 0 9px 28px 8px rgba(0, 0, 0, 0.05);
`;
```

修改createMessage，增加config功能，同时制作默认配置选项，到时候进行合并：

````typescript
export type MessageType =
	| "info"
	| "success"
	| "error"
	| "warning"
	| "loading"
	| "default";

export interface MessageConfig {
	/** 挂载点*/
	mount: HTMLElement;
	/** 动画延迟时间 */
	delay: number;
	/** 结束后回调 */
	callback: any;
	/** 动画持续时间 */
	animationDuring: number;
	/** 底色*/
	background: string;
	/** 文字颜色*/
	color: string;
}

const defaultConfig: MessageConfig = {
	mount: document.body,
	delay: 2000,
	callback: null,
	animationDuring: 300,
	background: color.lightest,
	color: color.dark,
};

let wrap: HTMLElement;
export const createMessage = (type: MessageType) => {
	return (content: ReactNode, config: Partial<MessageConfig> = {}) => {
		const fconfig = { ...defaultConfig, ...config };
		if (!wrap) {
			//如果有的话，说明已经调用过这个函数了，这个空div就可以一直复用
			wrap = document.createElement("div");
			wrap.style.cssText = `line-height:
		1.5;text-align:
		center;color: #333;
		box-sizing: border-box;
		margin: 0;
		padding: 0;
		list-style: none;
		position: fixed;
		z-index: 100000;
		width: 100%;
		top: 16px;
		left: 0;
		pointer-events: none;`;
			if (wrap) {
				fconfig.mount.appendChild(wrap); //挂body上
			}
		}

		const divs = document.createElement("div");
		wrap.appendChild(divs);
		ReactDom.render(
			<Message
				rootDom={wrap}
				parentDom={divs}
				content={content}
				fconfig={fconfig}
				iconType={type}
			/>,
			divs
		);
	};
};
````

styled element:

```typescript
const MessageText = styled.span<{ bg: string; fc: string }>`
	display: inline-block;
	padding: 10px 16px;
	background: ${(props) => props.bg};
	color: ${(props) => props.fc};
	font-size: ${typography.size.s2}px;
	font-weight: ${typography.weight.bold};
	margin: 10px;
	${messageBoxShadow};
	border-radius: 2px;
`;

const IconWrapper = styled.span<{ spin?: boolean }>`
	margin-right: 10px;
	& > svg {
		font-size: ${typography.size.s2}px;
		${(props) =>
			props.spin &&
			css`
				animation: ${iconSpin} 2s linear infinite;
			`}
	}
`;

const MessageTextWrapper = styled.div<{
	openState: boolean;
	closeState: boolean;
	ani: number;
}>`
	${(props) =>
		props.openState &&
		css`
			animation: ${messageOpenAnimate} ${props.ani / 1000}s ease-in;
		`}
	${(props) =>
		props.closeState &&
		css`
			animation: ${messageCloseAnimate} ${props.ani / 1000}s ease-in;
		`}
`;
```

主要页面逻辑：

```typescript
export type MessageProps = {
	rootDom: HTMLElement; //这个用来干掉parentDom 这个可以常驻
	parentDom: Element | DocumentFragment; //这个是挂载点 要unmount卸载 完毕后卸载挂载点 注意！一共2步卸载，别漏了
	content: ReactNode;
	fconfig: MessageConfig;
	iconType: MessageType;
};
export function Message(props: MessageProps) {
	const { rootDom, parentDom, content, fconfig, iconType } = props;
	const [close, setClose] = useState(false);

	const renderIcon = useMemo(() => {
		switch (iconType) {
			case "default":
				return null;
			case "info":
				return (
					<IconWrapper>
						<Icon icon="info" color={color.primary}></Icon>
					</IconWrapper>
				);
			case "success":
				return (
					<IconWrapper>
						<Icon icon="check" color={color.positive}></Icon>
					</IconWrapper>
				);
			case "error":
				return (
					<IconWrapper>
						<Icon icon="closeAlt" color={color.negative}></Icon>
					</IconWrapper>
				);
			case "warning":
				return (
					<IconWrapper>
						<Icon icon="info" color={color.warning}></Icon>
					</IconWrapper>
				);
			case "loading":
				return (
					<IconWrapper spin={true}>
						<Icon icon="sync"></Icon>
					</IconWrapper>
				);
			default:
				return null;
		}
	}, [iconType]);

	const unmount = useMemo(() => {
		return () => {
			if (parentDom && rootDom) {
				unmountComponentAtNode(parentDom);
				rootDom.removeChild(parentDom);
			}
		};
	}, [parentDom, rootDom]);

	useEffect(() => {
		//结束操作
		let closeStart = fconfig.delay - fconfig.animationDuring;
		let timer1 = window.setTimeout(
			() => {
				setClose(true);
			},
			closeStart > 0 ? closeStart : 0
		);
		let timer2 = window.setTimeout(() => {
			setClose(false);
			unmount();
			if (fconfig.callback) {
				fconfig.callback();
			}
		}, fconfig.delay);
		return () => {
			window.clearTimeout(timer1);
			window.clearTimeout(timer2);
		};
	}, [unmount, fconfig]);
	return (
		<MessageTextWrapper
			openState={true}
			closeState={close}
			ani={fconfig.animationDuring}
		>
			<MessageText bg={fconfig.background} fc={fconfig.color}>
				{renderIcon}
				{content}
			</MessageText>
		</MessageTextWrapper>
	);
}
```

导出最终可以给用户使用的message：

```typescript
export const message = {
	info: createMessage("info"),
	success: createMessage("success"),
	error: createMessage("error"),
	warning: createMessage("warning"),
	loading: createMessage("loading"),
	default: createMessage("default"),
};
```

### 编写story

同样的，做个knobstory:

```typescript
const Options: MessageType[] = [
	"info",
	"success",
	"error",
	"warning",
	"loading",
	"default",
];

export const knobsMessage = () => {
	const se = select<MessageType>("iconType", Options, "default");
	const op = {
		delay: number("delay", 2000),
		animationDuring: number("animationDuring", 300),
		background: color("background", "#fff"),
		color: color("color", "#333"),
	};
	const tx = text("content", "hello message");
	const onClick = () => message[se](tx, op);

	return (
		<div>
			<Button onClick={onClick}>click</Button>
		</div>
	);
};
```

再加个callback例子：

```typescript
export const callbackTest = () => (
	<div>
		<Button
			onClick={() =>
				message.loading("加载中", {
					callback: () => message.success("加载完成"),
				})
			}
		>
			callback
		</Button>
	</div>
);
```

还有个icon例子：

```typescript
export const withIcon = () => (
	<div>
		<Button
			onClick={() =>
				message.default(
					<span>
						<Icon icon="admin"></Icon>111
					</span>
				)
			}
		>
			callback
		</Button>
	</div>
);
```

基本完工。

### 编写测试

为了加快进度，测试好测的就测测给个快照，剩余未覆盖处自由发挥。

```typescript
import React from "react";
import { fireEvent, act } from "@testing-library/react";
import { message, MessageType, createMessage } from "../index";
import Button from "../../button";
import { unmountComponentAtNode, render } from "react-dom";

let container: HTMLDivElement;
beforeEach(() => {
	// 创建一个 DOM 元素作为渲染目标
	container = document.createElement("div");
	document.body.appendChild(container);
});
const clean = () => {
	// 退出时进行清理
	unmountComponentAtNode(container);
};
afterEach(() => clean());
const sleep = (delay: number) => {
	return new Promise((res) => {
		setTimeout(() => {
			res();
		}, delay);
	});
};
async function changeIcon(type: MessageType) {
	clean();
	act(() => {
		render(
			<Button
				id="btn"
				onClick={() => message[type](<span className="test3">11</span>)}
			>
				but
			</Button>,
			container
		);
	});
	const btn = container.querySelector("#btn");
	await act(async () => {
		fireEvent.click(btn!);
		await sleep(500);
	});
	expect(document.querySelector(".test3")).toBeTruthy();
	expect(container).toMatchSnapshot();
}
const fn = jest.fn();
describe("test Message component", () => {
	it(" render basic func ", async () => {
		act(() => {
			render(
				<Button
					id="btn"
					onClick={() =>
						message.default(<span className="test">11</span>)
					}
				>
					but
				</Button>,
				container
			);
		});
		const btn = container.querySelector("#btn");
		await act(async () => {
			fireEvent.click(btn!);
			await sleep(500);
		});

		expect(document.querySelector(".test")).toBeTruthy();
		expect(container).toMatchSnapshot();
		await act(async () => {
			await sleep(1500);
			expect(document.querySelector(".test")).toBeNull();
			expect(container).toMatchSnapshot();
		});
	});
	it("can change color", async () => {
		act(() => {
			render(
				<Button
					id="btn"
					onClick={() =>
						message.default(<span className="test2">22</span>, {
							background: "blue",
							color: "red",
						})
					}
				>
					but2
				</Button>,
				container
			);
		});
		const btn = container.querySelector("#btn");
		await act(async () => {
			fireEvent.click(btn!);
			await sleep(500);
		});
		expect(document.querySelector(".test2")).toBeTruthy();
		expect(container).toMatchSnapshot();
	});
	it(" callback test  ", async () => {
		act(() => {
			render(
				<Button
					id="btn"
					onClick={() =>
						message.default(<span className="callback">22</span>, {
							callback: fn,
						})
					}
				>
					but
				</Button>,
				container
			);
		});
		const btn = container.querySelector("#btn");
		expect(fn).not.toHaveBeenCalled();
		await act(async () => {
			fireEvent.click(btn!);
			await sleep(2100);
		});
		expect(fn).toHaveBeenCalled();
		expect(container).toMatchSnapshot();
	});

	it("can change icon", async () => {
		await changeIcon("default");
		await changeIcon("error");
		await changeIcon("info");
		await changeIcon("loading");
		await changeIcon("success");
		await changeIcon("warning");
	});
	//prettier-ignore
	it("icon type default", async () => {
		act(() => {
			render(
				<div>
					{/* @ts-ignore */}
					<Button id="btn" onClick={() => createMessage("sdss")("fff")}
					>
						but
					</Button>
				</div>,
				container
			);
		});
		const btn = container.querySelector("#btn");
		await act(async () => {
			fireEvent.click(btn!);
			await sleep(2100);
		});
		expect(container).toMatchSnapshot();
	});

	it("animate duration", async () => {
		act(() => {
			render(
				<Button
					id="btn"
					onClick={() =>
						message.default(<span className="callback">22</span>, {
							animationDuring: 1000,
							delay: 3000,
						})
					}
				>
					but
				</Button>,
				container
			);
		});
		let btn = container.querySelector("#btn");
		await act(async () => {
			fireEvent.click(btn!);
			await sleep(2100);
		});
		expect(container).toMatchSnapshot();
		await act(async () => {
			await sleep(1100);
		});
		expect(container).toMatchSnapshot();
	});
	it("animation lg delay", async () => {
		act(() => {
			render(
				<Button
					id="btn"
					onClick={() =>
						message.default(<span className="callback">22</span>, {
							animationDuring: 2000,
							delay: 1000,
						})
					}
				>
					but
				</Button>,
				container
			);
		});
		let btn = container.querySelector("#btn");
		await act(async () => {
			fireEvent.click(btn!);
			await sleep(1000);
		});
		expect(container).toMatchSnapshot();
	});
});
```
图片补充下keyframe动画的代码：
![image](http://img.zhufengpeixun.cn/07.png)
